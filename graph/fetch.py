import sys
from zipfile import ZipFile

import requests

url = 'https://linqs-data.soe.ucsc.edu/public/datasets/pubmed-diabetes/pubmed-diabetes.zip'
filename = 'diabetes.zip'

with open(filename, "wb") as f:
    response = requests.get(url, stream=True)
    total_length = response.headers.get('content-length')

    if total_length is None:
        f.write(response.content)
    else:
        dl = 0
        total_length = int(total_length)
        sys.stdout.write("Downloading pubmed-diabetes:\n")
        for data in response.iter_content(chunk_size=4096):
            dl += len(data)
            f.write(data)
            done = int(50 * dl / total_length)
            sys.stdout.write("\r[%s%s]" % ('=' * done, ' ' * (50-done)) )
            sys.stdout.flush()

with ZipFile(filename, 'r') as zObject:
    zObject.extractall()
