// rbtree.h
// RBT -> Abreviação de Red-Black Tree
#include <stdbool.h>

// Estrutura de um nó
typedef struct rbt_noh rbt_noh;
struct rbt_noh 
{
  char color; // 'r' para rubro (red) e 'b' para negro (black)
  rbt_noh *parent;
  rbt_noh *left;
  rbt_noh *right;
  int key;
};

// Estrutura auxiliar para printar os troncos da árvore.
typedef struct rbt_trunk rbt_trunk;
struct rbt_trunk
{
  rbt_trunk *prev;
  char *str;
};

rbt_noh *rbt_init();
// Executa as rotinas de inicialização da árvore.

rbt_noh *rbt_search(rbt_noh *root, int key);
// Realiza a busca na árvore por uma chave especificada. Retorna um ponteiro
// para o nó que contem essa chave (nil p/ o caso de não existir a chave na
// árvore).

rbt_noh *rbt_insert(rbt_noh *root, int key);
// Insere um nó na árvore com uma dada chave. Retorna a raiz da árvore.

rbt_noh *rbt_insert_fixup(rbt_noh *root, rbt_noh *current_noh);
// Função auxiliar que checa as condições de contorno da árvore para um dado nó
// após sua inserção e executa operações de rebalanceamento caso necessário.
// Retorna a raiz da árvore.

rbt_noh *rbt_remove(rbt_noh *root, int key);
// Remove um nó da árvore para uma dada chave. Retorna a raiz da árvore

rbt_noh *rbt_transplant(rbt_noh *root, rbt_noh *u, rbt_noh *v);
// Função auxiliar utilizada na remoção de nós. Substitui a subárvore com
// raiz no nó 'u' pela subárvore com raiz no nó 'v'.

rbt_noh *rbt_remove_fixup(rbt_noh *root, rbt_noh *x, rbt_noh *x_parent);
// Função auxiliar que checa as condições de contorno da árvore em um dado nó
// 'x' após a operação de deleção e executa operações de rebalanceamento caso
// necessário. Retorna a raiz da árvore.

rbt_noh *rbt_rotate_left(rbt_noh *root, rbt_noh *current_noh);
// Executa a rotação do nó corrente para a esquerda e retorna a raiz da árvore
// após a operação.
// 
// Executado a rotação à esquera no nó 1, por exemplo:
//            .————(subárvore1)
//      .————(2)
//     |      `————(subárvore2)
//—————(1)
//      `————(0)
//      
// Resulta em:
//      .————(subárvore1)
//—————(2)
//     |      .————(subárvore2)
//      `————(1)
//            `————(0)

rbt_noh *rbt_rotate_right(rbt_noh *root, rbt_noh *current_noh);
// Executa a rotação do nó corrente para a direita e retorna a raiz da árvore
// após a operação.
// 
// Executado a rotação à direita no nó 1, por exemplo:
//            
//      .————(2)
//—————(1)
//     |      .————(subárvore1)
//      `————(0)
//            `————(subárvore2)
//      
// Resulta em:
//            .————(2)
//      .————(1)
//     |      `————(subárvore1)
//—————(0)
//      `————(subárvore2)

void rbt_print_trunks(rbt_trunk* p);
// Função auxiliar utilizada por rbt_print para exibir as identações e os
// troncos da árvore em uma linha.
// 
// Os troncos podem ser entendidos como segmentos que antecedem a exibição do
// elemento corrente, por exemplo:
//      `————(173)
// ^~~~/^~~~/
// |    |
// |    Inicio do tronco 2
// Inicio do tronco 1
//
// A função é chamada recursivamente, para cada qual o nível de profundidade
// na árvore aumenta em um.

void rbt_print(rbt_noh *root, rbt_noh *nil, rbt_trunk *t_prev, bool is_right);
// Exibe no terminal a árvore passada como argumento.
// 
// root: Árvore que se deseja exibir
// *nil: Ponteiro referenciando o elemento nulo da árvore
// *t_prev: Tronco inicializado na chamada anterior
// is_right: Indicador da posição do nó referente a seu pai
//
// Exemplo de representação:
//      .————(930)
//     |      `————(705)
//     |            `————(453)
//—————(365)
//     |      .————(317)
//      `————(173)



