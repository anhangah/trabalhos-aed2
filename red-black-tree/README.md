# Red-Black Tree
Neste diretório se encontra a implementação da árvore rubro-negra.

- `rbt.h` Declaração das funções que implementam a árvore.
- `rbt.c` Implementação.
- `main.c` Interface CLI para manipulação de uma árvore.
 
## Instruções de build
Para build da árvore basta entrar neste diretório e executar
```
$ make
```

### Dependências
- `gcc` Testado com versão 10.x
- `make`

## Instruções de uso
Para executar o programa, apenas execute o binário gerado após o processo
de build. Passe os parâmetros `-h` ou `--help` para visualizar as instruções
de uso.
```
$ ./rbt --help
```

O programa possui dois modos de operação: interativo e não-interativo.
No primeiro, é exibido um menu de comandos, onde é possível escolher operações
na árvore, bem como os argumentos dessas operações. No modo não-interativo,
especificado como modo aleatório, ocorre apenas a inserção de multiplos
elementros aleatório, sendo esta quantidade definida por argumentos
passados na invocação do programa.

## Exemplos
### Modo aleatório (não-interativo)
```
$ ./rbt -p -r 10
```
Irá inserir 10 chaves aleatórias na árvore, exibindo-a na saída padrão após
cada inserção.

### Modo interativo
```sh
$ ./rbt
:i 10    # Insere 10 na árvore.
:i 20    # Insere 20 na árvore.
:i 30    # Insere 30 na árvore.
:r 20    # Remove 20 da árvore.
:a       # Insere um número aleatório na árvore.
:p       # Exibe a árvore.
:q       # Sai do programa.
```
