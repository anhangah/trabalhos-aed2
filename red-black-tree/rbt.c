#include "rbt.h"
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

rbt_noh *rbt_init()
{
  // Nó sentinela. Todos ponteiros dos nós folhas apontam para ele.
  rbt_noh *nil = malloc(sizeof(rbt_noh));
  nil->color = 'b';
  nil->parent = nil;
  nil->left = nil->right = NULL;

  return nil;
}

rbt_noh *rbt_search(rbt_noh *root, int key)
{
  rbt_noh *nil = root->parent;
  rbt_noh *x = root;

  while(x != nil && key != x->key)
    x = (key < x->key) ? x->left : x->right; 

  return x;
}

rbt_noh *rbt_insert(rbt_noh *root, int key)
{
  // Obtem o nó nulo
  rbt_noh *nil = root->parent;

  // Cria o novo nó com a key especificada
  rbt_noh *new_noh = malloc(sizeof(rbt_noh));
  new_noh->key = key;
  new_noh->color = 'r';
  new_noh->left = new_noh->right = new_noh->parent = nil;

  // Nó selecionado para ser o pai do novo nó.
  rbt_noh *parent_noh = nil;

  // Nó temporário para percorrer a árvore.
  rbt_noh *tmp = root;

  // Percorre a árvore até encontrar um nó nil e seleciona seu antecessor em
  // parent_noh.
  while(tmp != nil)
  {
    parent_noh = tmp;
    tmp = (key < tmp->key) ? tmp->left : tmp->right;
  }

  // Caso em que se insere o primeiro elemento.
  if(parent_noh == nil)
    {
      root = new_noh;
    }
  // Insere o novo nó à direita ou à esquerda do nó selecionado dependendo de
  // seu valor.
  else if(key < parent_noh->key)
    {
      parent_noh->left = new_noh;
      new_noh->parent = parent_noh;
    }
  else
    {
      parent_noh->right = new_noh;
      new_noh->parent = parent_noh;
    }

  // Retorna a raiz da árvore após a chamada da função que corrige violações
  // das propriedades da árvore após uma inserção.
  return rbt_insert_fixup(root, new_noh);
}

rbt_noh *rbt_insert_fixup(rbt_noh *root, rbt_noh *current_noh)
{
  rbt_noh *tmp;

  // Duas das possíveis violações que podem decorrer da inserção de elementos
  // na árvore são:
  //   - Dois nós vermelhos consecutivos
  //   - Nó raiz vermlho
  // O primeiro caso é tratado no atual loop while, enquanto a segunda possível
  // violação é corrigida sempre através da atribuição da cor preto ao nó raiz
  // em qualquer condição.

  // Enquanto a árvore violar a propriedade de não haver dois nós
  // vermelhos consecutivos (lembrando que o nó inserido sempre é vermelho
  // inicialmente):
  while(current_noh->parent->color == 'r')
    {
      // Se o nó pai de current_noh está à esquerda de seu avô:
      if (current_noh->parent == current_noh->parent->parent->left)
        {
          // tmp representa o nó tio.
          tmp = current_noh->parent->parent->right;

          // Caso o nó tio também seja vermelho, caímos no caso em que podemo
          // recolorir ambos para preto e recolorir o nó avô para vermelho sem
          // violar nenhuma propriedade da árvore. Isso pois, visto que o nó
          // avô já seria preto, o número de nós pretos em um caminho a partir
          // do nó avô continua o mesmo. Vale ressaltar que não precisamos nos
          // preocupar ao recolorir os nós tio e pai para preto pois a única
          // propriedade que isso poderia violar é a anteriormente citada ou
          // então a propriedade do nó raiz sempre precisar ser preto, mas
          // a primeira é recompensada com a recoloração do nó avô e a segunda
          // é tratada mais à frente recolorindo o nó raiz para preto.
          if (tmp->color == 'r')
            {
              current_noh->parent->color = 'b';
              tmp->color = 'b';
              current_noh->parent->parent->color = 'r';
              current_noh = current_noh->parent->parent;
            }
          // Caso o nó tio seja preto, não podemos simplesmente recolorir os
          // nós, necessitando de uma ou duas rotações junto das mudanças de
          // cores para corrigir as violações. Nesta situação, realizamos a
          // rotação à direita do nó avô e recolorimos ele para vermelho e o
          // nó pai para preto. Caso o nó inserido não esteja à direita de seu
          // pai, rotacionamos ele à esquerda e tratamos o pai (que agora é
          // filho) como se fosse o nó inserido. Como ambos são vermelhos,
          // não se viola nenhuma propriedade que já não estivesse violada
          // antes da rotação.
          else 
            {
              // Rotaciona o nó pai à esquerda. Desta forma, o nó corrente
              // passa a ser o antigno nó pai e o antigo nó corrente passa
              // a ser o pai. Continuamos na mesma situação de antes, porem
              // como se tivessemos inserido o nó pai depois de ter inserido
              // o nó que realmente foi inserido.
              if (current_noh == current_noh->parent->right)
                {
                  current_noh = current_noh->parent;
                  root = rbt_rotate_left(root, current_noh);
                }
              // Como este procedimento é executado para o caso do tio ser um
              // nó preto e o nó corrente e seu pai serem vermelhos, realiza-se
              // recolore-se o nó pai para preto e o avô para vermelho, pois assim,
              // após a rotação, teremos o nó pai sendo pai do que antes era o avô,
              // mas com a diferença de que agora não teremos nenhum nó vermelho
              // consecutivo.
              current_noh->parent->color = 'b';
              current_noh->parent->parent->color = 'r';
              root = rbt_rotate_right(root, current_noh->parent->parent);
            }
        }
      // Se o nó pai de current_noh está à direita de seu avô:
      // Mesma coisa que o caso descrito acima, porém com as direções esquerda
      // e direita invertidas.
      else
        {
          tmp = current_noh->parent->parent->left;

          if (tmp->color == 'r')
            {
              current_noh->parent->color = 'b';
              tmp->color = 'b';
              current_noh->parent->parent->color = 'r';
              current_noh = current_noh->parent->parent;
            }

          else
            {
              if (current_noh == current_noh->parent->left)
                {
                  current_noh = current_noh->parent;
                  root = rbt_rotate_right(root, current_noh);
                }
              current_noh->parent->color = 'b';
              current_noh->parent->parent->color = 'r';
              root = rbt_rotate_left(root, current_noh->parent->parent);
            }
        }
    }
  // Por garantia, colore-se a raiz com a cor preta sempre.
  root->color = 'b';
  return root;
}

rbt_noh *rbt_remove(rbt_noh *root, int key)
{
  rbt_noh *nil = root->parent;

  // O nó a ser deletado é inicialmente selecionado através da busca na árvore
  // pela key desejada.
  rbt_noh *selected_noh = rbt_search(root, key);
  rbt_noh *y = selected_noh; // Nó auxiliar para execução das operações

  // Ponteiro para o nó que substituirá o nó removido ou o nó que será
  // selecionado para substituir o nó removido. (É o nó onde poderam
  // ocorrer potenciais violações das propriedades da árvore).
  rbt_noh *elected_noh;

  // Ponteiro para o pai do nó que subtituirá o nó removido. Necessário
  // por conta da função de fixup utilizar elected_noh->parent, que no caso
  // deste ser nil, não está definido. Portanto, elected_parent é passado
  // como argumento da função de fixup.
  rbt_noh *elected_parent;

  // A cor do nó auxiliar é utilizada na avaliação da condição para execução
  // de rbt_remove_fixup().
  char y_original_color = y->color;

  // Não faz nada caso não exista nó com a key especificada.
  if(selected_noh == nil)
    return root;

  // Se o nó a ser deletado não tiver filho à esquerda, o substitui pelo
  // seu filho à direita.
  if(selected_noh->left == nil)
    {
      elected_noh = selected_noh->right;
      elected_parent = selected_noh->parent;
      root = rbt_transplant(root, selected_noh, elected_noh);
    }
  // Se o nó a ser deletado não tiver filho à direita, o substitui pelo
  // seu filho à esquerda.
  else if(selected_noh->right == nil)
    {
      elected_noh = selected_noh->left;
      elected_parent = selected_noh->parent;
      root = rbt_transplant(root, selected_noh, elected_noh);
    }
  // Nota: os dois procedimentos acima só são válidos pois o nó que será eleito
  // sempre será vermelho.

  // Se nenhum nó filho do nó selecionado para remoção for nil, precisamos...
  else
    {
      // Obtem o sucessor de selected_noh e faz y apontar a ele.
      // 'x' é utilizado apenas para percorrer a árvore.
      rbt_noh *x = selected_noh->right;
      while(x != nil)
        {
          y = x;
          x = x->left;
        }

      y_original_color = y->color;

      // A não ser que 'y' seja exatamente o filho à direita do nó removido,
      // o nó 'y' é substituido pelo seu nó à direita, para este, por sua
      // vez, subtituir o nó removido. No caso de 'y' ser o filho direito
      // do nó removido, não há a necessidade de se executar todo esse
      // procedimento.
      elected_noh = y->right;
      if(y != selected_noh->right)
        {
          elected_parent = y->parent;
          root = rbt_transplant(root, y, elected_noh);
          y->right = selected_noh->right;
          y->right->parent = y;
        }
      else
      {
          elected_parent = y;
          elected_noh->parent = elected_parent;
      }

      // Substitui o nó removido por 'y'.
      root = rbt_transplant(root, selected_noh, y);
      y->left = selected_noh->left;
      y->left->parent = y;
      y->color = selected_noh->color; // Preserva a mesma cor do nó removido
    }

  free(selected_noh);

  // Condição em que ocorre a violação da árvore, pois se o nó 'y' era preto,
  // então a sua substituição poderá violará a propriedade do número constante
  // de nós pretos para qualquer caminho até um nó folha.
  if(y_original_color == 'b')
    root = rbt_remove_fixup(root, elected_noh, elected_parent);

  return root;
}

rbt_noh *rbt_remove_fixup(rbt_noh *root, rbt_noh *x, rbt_noh *x_parent)
{
  rbt_noh *nil = root->parent;
  rbt_noh *w; // Nó temporário

  while(x != root && x->color == 'b')
    {
      if(x == x_parent->left)
        {
          // 'w' é irmão de x.
          w = x_parent->right;
          if(w->color == 'r') // Neste caso, x_parent é necessariamente preto
            {
              // Inverte as cores do pai e do irmão de 'x' e rotaciona o pai.
              // Desta forma, 'w' passa a ser pai de 'x'.
              w->color = 'b';
              x_parent->color = 'r';
              root = rbt_rotate_left(root, x_parent);
              w = x_parent->right; // 'w' aponta ao irmão de 'x' novamente
            }
          if(w->left->color == 'b' && w->right->color == 'b')
            {
              w->color = 'r';
              x = x_parent;
              x_parent = x->parent;
            }
          else
            {
              // Caso os filhos sejam um veremlho e um preto, realiza o
              // procedimento para cair no caso dos dois serem vermelhos.
              if(w->right->color == 'b')
               {
                 w->left->color = 'b';
                 w->color = 'r';
                 root = rbt_rotate_right(root, w);
                 w = x_parent->right; // 'w' aponta ao irmão de 'x' novamente
               }

              // A partir daqui, ambos filhos de 'w' são vermelhos.
              w->color = x_parent->color;
              x_parent->color = 'b';
              w->right->color = 'b';
              root = rbt_rotate_left(root, x_parent);
              x = root;
              x_parent = nil;
            }
        }
      // Mesmo procedimento acima, porém com left e right invertidos.
      else
        {
          w = x_parent->left;
          if(w->color == 'r')
            {
              w->color = 'b';
              x_parent->color = 'r';
              root = rbt_rotate_right(root, x_parent);
              w = x_parent->left;
            }
          if(w->right->color == 'b' && w->left->color == 'b')
            {
              w->color = 'r';
              x = x_parent;
              x_parent = x->parent;
            }
          else
            {
              if(w->left->color == 'b')
               {
                 w->right->color = 'b';
                 w->color = 'r';
                 root = rbt_rotate_left(root, w);
                 w = x_parent->left;
               }

              w->color = x_parent->color;
              x_parent->color = 'b';
              w->left->color = 'b';
              root = rbt_rotate_right(root, x_parent);
              x = root;
              x_parent = nil;
            }
        }
    }
  x->color = 'b';
  return root;
}

rbt_noh *rbt_transplant(rbt_noh *root, rbt_noh *u, rbt_noh *v)
{
  rbt_noh *nil = root->parent;

  // O único nó cujo pai é nil é o nó raiz.
  if(u->parent == nil)
    root = v;

  else if(u == u->parent->right)
    u->parent->right = v;

  else // u == u->parent->left
    u->parent->left = v;

  if(v != nil)
    v->parent = u->parent;

  return root;
}


rbt_noh *rbt_rotate_left(rbt_noh *root, rbt_noh *current_noh)
{
  rbt_noh *nil = root->parent;
  rbt_noh *parent;

  // Nó que substituirá o nó rotacionado (que foi passado como argumento da
  // função).
  rbt_noh *replacement_noh;

  replacement_noh = current_noh->right;
  parent = current_noh->parent;

  // Direita do nó a ser rotacionado aponta para a esquerda do nó selecionado,
  // pois o ponteiro left do nó selecionador deverá apontar ao nó corrente.
  current_noh->right = replacement_noh->left;

  if(current_noh->right != nil)
    current_noh->right->parent = current_noh;

  replacement_noh->parent = parent;

  // O único nó que o ponteiro parent aponta p/ nil é a raiz.
  if(parent == nil)
    root = replacement_noh;

  else if(parent->left == current_noh)
    parent->left = replacement_noh;

  else if(parent->right == current_noh)
    parent->right = replacement_noh;

  replacement_noh->left = current_noh;
  current_noh->parent = replacement_noh;

  return root;
}

rbt_noh *rbt_rotate_right(rbt_noh *root, rbt_noh *current_noh)
// Mesma coisa que rbt_rotate_left, porém com as direções esquerda
// e direita invertidas.
{
  rbt_noh *nil = root->parent;
  rbt_noh *replacement_noh, *parent;

  replacement_noh = current_noh->left;
  parent = current_noh->parent;
        
  current_noh->left = replacement_noh->right;
  if(current_noh->left != nil)
    current_noh->left->parent = current_noh;

  replacement_noh->parent = parent;

  if(parent->left == current_noh)
    parent->left = replacement_noh;

  else if(parent->right == current_noh)
    parent->right = replacement_noh;

  else
    root = replacement_noh;

  replacement_noh->right = current_noh;
  current_noh->parent = replacement_noh;

  return root;
}

void rbt_print_trunks(rbt_trunk *p)
// Supondo a seguinte árvore:
//       .————(2)
// —————(1)
//       `————(0)
//       
// A função é chamada três vezes (1x para cada elemento da árvore), sem contar
// as chamadas recursivas. Para o elemento 0, por exemplo, a função é
// inicialmente chamada pela função rbt_print() com o ponteiro *p contendo
// p->str = "`————" e p->prev->str = "    ".
{
  if(!p) return;

  rbt_print_trunks(p->prev);
  printf("%s", p->str); 
}

void rbt_print(rbt_noh *root, rbt_noh *nil, rbt_trunk *t_prev, bool is_right)
{
  // Condição de parada para as chamadas recursivas
  if(root == nil) return;

  // Inicializa o tronco corrente com epaço em branco
  rbt_trunk *t = malloc(sizeof(rbt_trunk));
  t->prev = t_prev;
  t->str = "      ";
  // t->str: String que precede o elemento corrente

  // Chama recursiva a subárvore à direita
  rbt_print(root->right, nil, t, 1);

  // O único caso onde *t_prev é NULL é quando o elemento corrente é a raiz
  if(!t_prev)
    t->str = "—————";

  // No caso do elemento corrente estar à direita de seu pai, necessariamente
  // t_prev->str é espaço em branco, vide a inicialização do tronco antes da
  // chamada recursiva para a subárvore à direita
  else if(is_right)
    {
      t->str = ".————";
    }
  else 
    {
      t->str = "`————";
      t_prev->str = "      ";
    }

  // Exibe a linha completa para o elemento corrente
  rbt_print_trunks(t);
  (root->color == 'r') ? printf("\e[1;31m") : printf("\e[1;90m");
  printf("(%d)\e[0m\n", root->key);


  // Caso o elemento esteja à direita de seu pai, a string do tronco
  // antecessor será "     |" para a chamada do filho a esquerda. Caso este
  // não exista, esta definição não terá efeito.
  if(is_right) t_prev->str = "     |";

  // Chama a função para o filho a esquerda definindo o tronco corrente.
  // Semelhante ao caso acima, a definição só terá efeito no caso do filho
  // esquerdo ter um filho à sua direita. Caso contrário, t_prev->str é
  // redefinido.
  t->str = "     |";
  rbt_print(root->left, nil, t, 0);

  free(t);
}
